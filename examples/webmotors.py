
import json

from urllib.parse import urlencode
from robot import Robot
from robot.collector.shortcut import *
from robot.core import ContextImpl
import logging

logging.basicConfig(level=logging.DEBUG)

url = 'https://www.webmotors.com.br/api/search/car?' + urlencode({
    'url': 'https://www.webmotors.com.br/carros/estoque?lkid=1022',
    'showMenu': 'false',
    'showCount': 'true',
    'showBreadCrumb': 'false',
    'testAB': 'false',
    'returnUrl': 'false',
    'actualPage': '2',
})

http_headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
}

collector = pipe(
    const(url),
    get(headers=http_headers),
    fn(lambda it: print(json.dumps(it, indent=2))),
)



with Robot() as robot:
    robot.sync_run(collector)